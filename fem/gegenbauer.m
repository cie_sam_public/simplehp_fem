function result = gegenbauer(x,i,rho)
t = i-1;
switch i
    case 0, result = 1;
    case 1, result = 2*rho*x;
    otherwise        
        table=zeros(i+1,1);
        table(0+1) = 1;
        table(1+1) = 2*rho*x;
        for k=1:i-1
            table(k+1+1) = 1.0/(k+1)*(2*(k+rho)*x*table(k+1) - ...
                (k+2*rho-1)*table(k-1+1));
        end
        result = table(i+1);
end