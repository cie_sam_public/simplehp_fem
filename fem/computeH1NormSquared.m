% Compute H1 norm of function with given functionDu
function result=computeH1NormSquared(G,constants)
result=zeros(1,size(G.elementsVertices,1));
parfor c=1:size(G.elementsVertices,1)
    if ~G.elementsInMesh(c); continue; end
    [x1,w1]=lgwt(1*(10+G.elementsAssemblyDegree(c,1)),-1,1);
    [x2,w2]=lgwt(1*(10+G.elementsAssemblyDegree(c,2)),-1,1);
    for r=1:length(x1)
        for s=1:length(x2);
            vs=Psi([x1(r),x2(s)],c,G,constants);
            determinant=abs(det(DPsi(x1(r),x2(s),c,G)));
            result(c)=result(c)+w1(r)*w2(s) ...
               *functionMaterialX(vs(1))*functionMaterialY(vs(2))...
               *(functionDu(vs(1),vs(2)))*(functionDu(vs(1),vs(2)))' ...
               *determinant;   
        end
    end
end
result=sum(result);
end