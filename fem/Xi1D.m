% 1D local basis functions
function result=Xi1D(x,I,p)
result=zeros(length(I),1);
for k=1:length(I)
    i=I(k);
    switch i
        case 0, result(k)=0.5*(1-x);
        case 1, result(k)=0.5*(1+x);
        otherwise
            rho=-0.5;            
            table=zeros(i+1,1);
            table(0+1) = 1;
            table(1+1) = 2*rho*x;
            for m=1:i-1
                table(m+1+1) = 1.0/(m+1)*(2*(m+rho)*x*table(m+1) - ...
                    (m+2*rho-1)*table(m-1+1));
            end
            result(k)=table(i+1);
    end
end
end