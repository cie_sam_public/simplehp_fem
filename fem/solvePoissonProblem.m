function [G,conMats,u,h1RelativeError]=solvePoissonProblem(conMats,G,constants)
disp('Assembling')
[globalStiffnessMatrix,loadVector,G]=assemble(G,constants,conMats);
% Homogeneous Dirichlet conditions
G.dirichletDofs=false(1,G.numberOfDofs);
for e=1:size(G.edgesVertices,1)
    if ~(G.edgesInMesh(e) && G.edgesDirichlet(e)); continue; end
    % set edge's endpoints' dof entries
    endpoints=G.verticesDofNumber(G.edgesVertices(e,:));
    G.dirichletDofs(endpoints)=ones(size(endpoints));
    % does edge have its own global dofs?
    globalDof=G.edgesDofNumber(e); if globalDof==0; continue; end
    for l=0:G.edgesOrientedDegree(e)-2; G.dirichletDofs(globalDof+l)=1; end
end
G.dirichletDofs=find(G.dirichletDofs);
freeNodes=setdiff(1:G.numberOfDofs,G.dirichletDofs);
A=globalStiffnessMatrix(freeNodes,freeNodes);
u=zeros(G.numberOfDofs,1);
u(freeNodes)=pcg(A,loadVector(freeNodes),1e-15,10000,diag(diag(A)));
% Compute error
h1NormSquared=computeH1NormSquared(G,constants);
h1Error=sqrt(abs(h1NormSquared-u'*globalStiffnessMatrix*u));
h1RelativeError=100*h1Error/sqrt(h1NormSquared);
fprintf('Relative H1/Energy error in percent = %f\n',h1RelativeError);
end