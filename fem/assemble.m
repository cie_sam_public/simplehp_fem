function [globalStiffnessMatrix,loadVector,G]=assemble(G,constants,conMats)
globalStiffnessMatrix=sparse(G.numberOfDofs,G.numberOfDofs);
loadVector=zeros(G.numberOfDofs,1);
% Tabularize calls to Xi and DXi
NUM_ADDITIONAL_KNOTS=10; % Choose it >= 1
maxDegree=10+max(max(G.elementsAssemblyDegree));
table_Xis=zeros(maxDegree,maxDegree+1,maxDegree+NUM_ADDITIONAL_KNOTS);
table_DXis=zeros(maxDegree,maxDegree+1,maxDegree+NUM_ADDITIONAL_KNOTS);
for p=1:maxDegree
  [x,~]=lgwt(1*(NUM_ADDITIONAL_KNOTS+p),-1,1); % (*)
  for rows=1:p+1
    for l=1:p+NUM_ADDITIONAL_KNOTS
      table_Xis(p,rows,l)=Xi1D(x(l),rows-1,p);
      table_DXis(p,rows,l)=DXi1D(x(l),rows-1,p);
    end
  end
end
G.table_Xis=table_Xis; G.table_DXis=table_DXis;
plus=@(x,y) x+y; % In MATLAB's parfor, sparse plus operations might get converted to full (leading to out-of-memory errors)
parfor c=1:size(G.elementsVertices,1)
  if ~G.elementsInMesh(c); continue; end
  p=G.elementsAssemblyDegree(c,1); q = G.elementsAssemblyDegree(c,2);
  % Local stiffness matrix
  [x1,w1]=lgwt(NUM_ADDITIONAL_KNOTS+p,-1,1); % Needs to be the same as (*)
  [x2,w2]=lgwt(NUM_ADDITIONAL_KNOTS+q,-1,1);  
  localStiffnessMatrix=sparse((p+1)*(q+1),(p+1)*(q+1));  
  localLoadVector=zeros((p+1)*(q+1),1);
  matL=L(p,q);  
  % load vector:
  for r=1:length(x1)
    for s=1:length(x2);
      vs=Psi([x1(r),x2(s)],c,G,constants);
      determinant=abs(det(DPsi(x1(r),x2(s),c,G)));
      for i=1:size(matL,1)
        localLoadVector(i)=localLoadVector(i) + ...
          w1(r)*w2(s)*table_Xis(p,matL(i,1)+1,r)*table_Xis(q,matL(i,2)+1,s)... 
          *G.functionF(vs(1),vs(2),G,constants) ...
          *determinant;
      end
    end
  end
  dpsi=DPsi(1,1,c,G); determinant=abs(det(dpsi)); jac=inv(dpsi); jac=jac*jac';  
  X=[x1(:) 0.5*ones(length(x1),1)];
  P=Psi(X,c,G,constants);
  val1=functionMaterialX(P(:,1));
  X=[0.5*ones(length(x2),1) x2(:)];
  P=Psi(X,c,G,constants)
  val2=functionMaterialY(P(:,2));
  for I=1:size(matL,1)
    tmp=matL(I,:); i1=tmp(1); i2=tmp(2);
    for J=1:size(matL,1)
      tmp=matL(J,:); j1=tmp(1); j2=tmp(2);
      wertIJ1=zeros(1,4); wertIJ2=zeros(1,4);
      for l=1:length(x1) %val1(l)
        wertIJ1(1)=wertIJ1(1)+w1(l)*table_DXis(p,i1+1,l)*table_DXis(p,j1+1,l)*val1(l);
        wertIJ1(2)=wertIJ1(2)+w1(l)*table_Xis(p,i1+1,l)*table_DXis(p,j1+1,l)*val1(l);
        wertIJ1(3)=wertIJ1(3)+w1(l)*table_DXis(p,i1+1,l)*table_Xis(p,j1+1,l)*val1(l);
        wertIJ1(4)=wertIJ1(4)+w1(l)*table_Xis(p,i1+1,l)*table_Xis(p,j1+1,l)*val1(l);
      end
      for l=1:length(x2)
        wertIJ2(1)=wertIJ2(1)+w2(l)*table_DXis(q,i2+1,l)*table_DXis(q,j2+1,l)*val2(l);
        wertIJ2(2)=wertIJ2(2)+w2(l)*table_Xis(q,i2+1,l)*table_DXis(q,j2+1,l)*val2(l);
        wertIJ2(3)=wertIJ2(3)+w2(l)*table_DXis(q,i2+1,l)*table_Xis(q,j2+1,l)*val2(l);
        wertIJ2(4)=wertIJ2(4)+w2(l)*table_Xis(q,i2+1,l)*table_Xis(q,j2+1,l)*val2(l);
      end
      productIndices=[1 4; 2 3; 3 2; 4 1];
      wert=0;
      for s=1:size(productIndices,1)
        ind=productIndices(s,:);
        wert=wert+jac(s)*wertIJ1(ind(1))*wertIJ2(ind(2));
      end
      localStiffnessMatrix(I,J)=localStiffnessMatrix(I,J) + wert*determinant;
    end
  end
  globalStiffnessMatrix = plus(globalStiffnessMatrix,conMats{c}*localStiffnessMatrix*conMats{c}');
  % Neumann boundary conditions: Integrals over edges
  [x,w]=lgwt(NUM_ADDITIONAL_KNOTS+max(G.elementsAssemblyDegree(c,:))+1,0,1);
  for e=1:4
    edge=G.elementsEdges(c,e);
    if length(G.edgesNeumann) >= edge && G.edgesNeumann(edge)
      [endpoint1,endpoint2,normal]=getEdgeEndpointsAndNormal(edge,e,G);
      for r=1:length(w)
        point=x(r)*(endpoint2-endpoint1)+endpoint1;
        tra=PsiInv(point,c,G);
        for i=1:size(matL,1)
          localLoadVector(i)=localLoadVector(i)+w(r)*functionG(point(1),point(2),normal) ...
            *Xi1D(tra(1),matL(i,1),p)*Xi1D(tra(2),matL(i,2),q) ...
            *norm(endpoint2 - endpoint1);
        end
      end
    end
  end
  loadVector=loadVector+conMats{c}*localLoadVector;
end
end
% Retrieve endpoints and correct oriented normal to boundary edge
function [endpoint1,endpoint2,normal]=getEdgeEndpointsAndNormal(edge,e,G)
coords=G.verticesCoordinates(G.edgesVertices(edge,:),:);
switch e
    case 1; endpoint1=coords(1,:); endpoint2=coords(2,:); diff=endpoint2-endpoint1; normal=[diff(2),-diff(1)];
    case 2; endpoint1=coords(1,:); endpoint2=coords(2,:); diff=endpoint2-endpoint1; normal=-[-diff(2),diff(1)];
    case 3; endpoint1=coords(1,:); endpoint2=coords(2,:); diff=endpoint2-endpoint1; normal=[-diff(2),diff(1)];
    case 4; endpoint1=coords(1,:); endpoint2=coords(2,:); diff=endpoint2-endpoint1; normal=-[diff(2),-diff(1)];
end
normal=normal/norm(normal);
end