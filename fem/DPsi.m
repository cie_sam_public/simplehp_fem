% Jacobian of Psi at a point (x,y)
function result=DPsi(x,y,c,G)
coordinates=G.verticesCoordinates(G.elementsVertices(c,:),:);
result=matrixA(coordinates);
end