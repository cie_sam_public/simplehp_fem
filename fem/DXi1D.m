function y=DXi1D(x,i,p,type,constants)
switch i
    case 0
        y = -0.5;
    case 1
        y = 0.5;
    otherwise
        rho = -0.5;
        y=2*(-0.5)*gegenbauer(x,i-1,rho+1);
end
end

