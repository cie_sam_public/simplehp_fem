function FEM=determineAssemblyDegree(FEM)
FEM.elementsAssemblyDegree=FEM.elementsDegree; % |\label{line:assemblyDegree:Elements}|
for e=find(FEM.edgesRegular)' % |\label{line:assemblyDegree:Edge}|
  [support,~]=getEdgeSupport_Large(e,FEM);
  for s=1:size(support,2)
    for T=support{s}
      FEM.elementsAssemblyDegree(T,:)=max([FEM.elementsAssemblyDegree(T,:);FEM.edgesDegree(e,:)],[],1);
    end
  end
end
for v=find(FEM.verticesRegular) % |\label{line:assemblyDegree:Vertex}|
  [support,~]=getVertexSupport_Large(v,FEM);
  for s=1:length(support)
    for T=support{s}
      FEM.elementsAssemblyDegree(T,:)=max([FEM.elementsAssemblyDegree(T,:);FEM.verticesDegree(v,:)],[],1);
    end
  end
end