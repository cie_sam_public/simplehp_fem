function result=Psi(x,c,G,constants)
result=zeros(size(x)); 
for j=1:size(x,1); result(j,:)=(G.A{c}*x(j,:)'+G.b{c}')'; end
end
 
