function result=L(q1,q2) % Matrix $L(q_1,q_2)$ |\label{line:matrixL}|
result=zeros((q1+1)*(q2+1),2); result(1:4,:) = [0,0; 1,0; 1,1; 0,1];
i=5;
for k=2:q1; result(i,:) = [k,0]; i=i+1; end; for l=2:q2; result(i,:) = [1,l]; i=i+1; end
for k=2:q1; result(i,:) = [k,1]; i=i+1; end; for l=2:q2; result(i,:) = [0,l]; i=i+1; end
for k=1:q1-1; for l=1:q2-1; result(i,:) = [1+k,1+l]; i=i+1; end; end
end 
