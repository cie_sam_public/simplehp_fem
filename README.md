<body>
<a name="Wiki"></a>

<h1 >
Wiki<a href="#Wiki" class="wiki-anchor"></a>

</h1>
<a name="Running-the-example-problem"></a>

<h3 >
Running the example
problem<a href="#Running-the-example-problem" class="wiki-anchor"></a>

</h3>
<p>
Note that <strong>MATLAB versions 2015 or newer</strong> are required to
run the program.

</p>
<p>
The L-shaped domain example with 10 refinements is implemented (and can
be located in the ./problems/ldomain\_exp folder).

</p>
<p>
To run it with in MATLAB, change to the project's main folder, and type
<em>main</em>.

</p>
<p>
The relative energy error in percent is computed and shown (along with
some informative output).

</p>
<p>
To change the number of refinements, alter the value assigned to
<em>parameters.iso</em> in the <em>main.m</em>.

</p>
</body>