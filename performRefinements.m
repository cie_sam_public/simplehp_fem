function G=performRefinements(G,constants,parameters)
if strcmp(parameters.problemShortname,'ldomain_exp')
    e1=1; e2=2; e3=3;
    LEVELS=parameters.levels;
    constants.ISO=parameters.iso;
    G.elementsDegree([1 2 3],:)=constants.ISO*ones(3,2);
    for i=1:parameters.iso-1 %  start with 3 elements=mesh size h=1
        G=multipleRefinements([e1 1],G,constants);
        e1=G.elementsEdgesSize - 3;
        G=multipleRefinements([e2 1],G,constants);
        e2=G.elementsEdgesSize - 2;
        G=multipleRefinements([e3 1],G,constants);
        e3=G.elementsEdgesSize - 1;
        for c=[e1 e2 e3]
            G.elementsDegree(c,:)=[constants.ISO-1 constants.ISO-1];
        end
        constants.ISO=constants.ISO-1;
    end
end