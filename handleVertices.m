function FEM=handleVertices(FEM)
for v=1:size(FEM.verticesCoordinates,1); FEM.verticesEdges{v}=[]; end
for e=find(FEM.edgesInMesh)' % Edges incident to vertices |\label{line:verticesEdges}|
  for endpoint=FEM.edgesVertices(e,:); FEM.verticesEdges{endpoint}(end+1)=e; end
end
FEM.verticesRegular=false(1,size(FEM.verticesCoordinates,1));
for v=1:length(FEM.verticesRegular) % Regularity of vertices |\label{line:verticesRegular}|
  isRegular=1;
  edges=FEM.verticesEdges{v}; edgesPairs=nchoosek(edges,2);
  for pair=edgesPairs'
    if ~isempty(intersect(FEM.edgesAncestors{pair(1)},FEM.edgesAncestors{pair(2)}))
      isRegular=0; break;
    end
  end
  FEM.verticesRegular(v)=isRegular;
end
