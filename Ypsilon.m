function [Y,z] = Ypsilon(smallElement,bigElement,G)
smallCoordinates=G.verticesCoordinates(G.elementsVertices(smallElement,:),:);
vs=PsiInv([smallCoordinates(1,:);smallCoordinates(3,:)],bigElement,G);
Y=zeros(2); a=0.5*(vs(2,:)-vs(1,:)); Y(1,1)=a(1); Y(2,2)=a(2); z=0.5*(vs(2,:)+vs(1,:));
end