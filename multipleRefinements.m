function  G=multipleRefinements(refs,G,constants)
for r=1:size(refs,1); G=refine(refs(r,1),refs(r,2),G,constants); end