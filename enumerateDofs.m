function FEM=enumerateDofs(FEM)
FEM.verticesDofNumber=zeros(size(FEM.verticesCoordinates,1),1); FEM.dofNumbersVertex=[];
FEM.edgesDofNumber=zeros(size(FEM.edgesRegular,1),1); FEM.dofNumbersEdge=[];
FEM.elementsDofNumber=zeros(size(FEM.elementsVertices,1),1); FEM.dofNumbersElement=[];
dofNumber=1;
for T=find(FEM.elementsInMesh)' % |\label{line:createDofs:Loop}|
  for v=1:4 % |\label{line:createDofs:Vertex}|
    vertex=FEM.elementsVertices(T,v);
    if FEM.verticesRegular(vertex) && FEM.verticesDofNumber(vertex)==0
      FEM.verticesDofNumber(vertex)=dofNumber; dofNumber=dofNumber+1;
    end
  end
  for e=1:4 % |\label{line:createDofs:Edge}|
    edge=FEM.elementsEdges(T,e);
    if FEM.edgesRegular(edge) && FEM.edgesOrientedDegree(edge)>1 && FEM.edgesDofNumber(edge,:)==0
      FEM.edgesDofNumber(edge)=dofNumber; dofNumber=dofNumber+FEM.edgesOrientedDegree(edge)-1;
    end
  end
  FEM.elementsDofNumber(T)=dofNumber; dofNumber=dofNumber+(FEM.elementsDegree(T,1)-1)*(FEM.elementsDegree(T,2)-1); % |\label{line:createDofs:Element}|
end
FEM.numberOfDofs=dofNumber-1;
