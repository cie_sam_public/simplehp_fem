function FEM=handleEdges(FEM)
FEM.edgesRegular=false(FEM.edgesVerticesSize,1);FEM.edgesInMesh=false(FEM.edgesVerticesSize,1);
% In-mesh edges
for e=1:size(FEM.edgesVertices,1)
    FEM.edgesOriginalNeighbors{e}=[0 0]; FEM.edgesNeighbors{e}=[0 0];
end
edgesInMesh=reshape(FEM.elementsEdges(FEM.elementsInMesh,:),[],1); % Find edges in the mesh |\label{line:edgesInMeshBegin}|
FEM.edgesInMesh(edgesInMesh)=ones(size(edgesInMesh)); % Mark them as in-mesh|\label{line:edgesInMeshEnd}|
% Ancestors of in-mesh edges 
FEM.edgesAncestors=cell(1,size(FEM.edgesVertices,1));
for e=1:length(FEM.edgesAncestors) % |\label{line:edgesAncestors}|
    FEM.edgesAncestors{e}=sparse(size(FEM.edgesVertices,1),1);
    if ~FEM.edgesParent(e); continue; end
    FEM.edgesAncestors{e}(FEM.edgesParent(e))=FEM.edgesInMesh(FEM.edgesParent(e));
    FEM.edgesAncestors{e}=or(FEM.edgesAncestors{e},FEM.edgesAncestors{FEM.edgesParent(e)});        
end
FEM.edgesAncestors=cellfun(@find,FEM.edgesAncestors,'UniformOutput',false);
% Regularity and neighbors of edges
LEFT=1; RIGHT=2; localEdgeNumbersNeighbor=[LEFT LEFT RIGHT RIGHT]; % Is T left or right neighbor? |\label{line:NeighborMapping}|
for c=find(FEM.elementsInMesh)' % Mark regular edges, find neighbors |\label{line:edgesInMeshLoop}|
    for e=1:4
        edge=FEM.elementsEdges(c,e);
        FEM.edgesNeighbors{edge}(localEdgeNumbersNeighbor(e))=c;
        edgesAncestors=FEM.edgesAncestors{edge};
        indexOfOldestAncestor=find(FEM.edgesInMesh(edgesAncestors),1,'last');% |\label{line:edgesOldestInMeshAncestor}|
        u=edgesAncestors(indexOfOldestAncestor);
        if isempty(u); FEM.edgesRegular(edge)=1;
        else
            FEM.edgesRegular(u)=1;
            if FEM.elementsInMesh(c); FEM.edgesNeighbors{u}(end+1)=c; end
        end
    end
end
% Original neighbors of edges 
for edge=find(FEM.edgesRegular)' % |\label{line:edgesOriginalNeighbors}|
    [localEdgeNumber,elementsWithEdge]=find(ismember(FEM.elementsEdges',edge));
    [localEdgeNumber,ind]=unique(localEdgeNumber,'stable'); elementsWithEdge=elementsWithEdge(ind);
    len=min(length(localEdgeNumber),2); % First (up to) 2 occurrences of edge
    FEM.edgesOriginalNeighbors{edge}(localEdgeNumbersNeighbor(localEdgeNumber(1:len)))= ...
        elementsWithEdge(1:len);
end