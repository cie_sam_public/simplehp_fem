function result=functionU(x,y)
[theta, r]=cart2pol(x,y);
if theta < 0; theta = theta + 2*pi; end % no negative angles
result=r^(2/3)*sin(2/3*theta);
end