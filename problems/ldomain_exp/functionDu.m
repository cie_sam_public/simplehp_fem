function result=functionDu(x,y)
[theta, r]=cart2pol(x,y);
if theta < 0; theta = theta + 2*pi; end % no negative angles
result=[(2/3).*(x.^2+y.^2).^(-2/3).*((-1).*y.*cos((2/3).*theta)+x.*sin(( ...
  2/3).*theta)),(2/3).*(x.^2+y.^2).^(-2/3).*(x.*cos((2/3).*theta)+ ...
  y.*sin((2/3).*theta))];
end