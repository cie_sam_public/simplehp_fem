function result=functionG(x,y,normal)
[theta,r]=cart2pol(x,y);
if theta < 0; theta = theta + 2*pi; end % no negative angles
result=2/3*r^(-4/3)*[x*sin(2/3*theta)-y*cos(2/3*theta) y*sin(2/3*theta)+x*cos(2/3*theta)]*normal';