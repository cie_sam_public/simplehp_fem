function G=finalizeRefinements(G,constants)
% All child coordinates are still given in reference coordinates; transform
% them to real-world coordinates. This is done from coordinates belonging
% to vertices on a lower refinement step to those on a higher refinement
% step, since level k+1 depends on the coordinates
% of level k. (Thus, the following cannot be PARFOR'ed unless performed
% level-wise.).
% If no parent is given, we assume the vertices are already transformed.
for i=1:G.verticesCoordinatesSize
    if G.verticesParentElement(i)==0; continue; end 
    G.A{G.verticesParentElement(i)}=matrixA(G.verticesCoordinates(G.elementsVertices(G.verticesParentElement(i),:),:));
    G.b{G.verticesParentElement(i)}=vectorB(G.verticesCoordinates(G.elementsVertices(G.verticesParentElement(i),:),:));    
    G.verticesCoordinates(i,:)=Psi(G.verticesCoordinates(i,:),G.verticesParentElement(i),G,constants);
end
% In the refinements, we did not care if vertices (verticesCoordinates),
% edges (edgesVertices) already existed in the mesh; thus, we need to
% remove duplicates.
% verticesCoordinates: Use uniquetol to straighten out all almost-duplicates of
% vertices
[uniqueCoordinates,~,indices]=uniquetol(G.verticesCoordinates(1:G.verticesCoordinatesSize,:),1e-14,'ByRows', true);
G.verticesCoordinates=uniqueCoordinates(indices,:);
% Now, remove all duplicates and also keep the order ('stable'):
[verticesCoordinatesUnique,~,vcl]=unique(G.verticesCoordinates,'rows','stable');
% As a consequence, the vertices in edgesVertices have changed:
G.edgesVertices=G.edgesVertices(1:G.edgesVerticesSize,:);
for i=1:G.edgesVerticesSize; G.edgesVertices(i,:)=vcl(G.edgesVertices(i,:)); end
% edgesVertices: Now, remove all duplicates of edges.
[edgesVerticesUnique,evl2,evl]=unique(G.edgesVertices,'rows','stable');
%   As a consequence, the edges in edgesParents have changed:
G.edgesParentNeu=G.edgesParent(evl2);
for i=1:length(G.edgesParentNeu)
    if G.edgesParentNeu(i)~=0; G.edgesParentNeu(i)=evl(G.edgesParentNeu(i)); end
end
G.edgesParent=G.edgesParentNeu;
% Remove duplicate vertices in elementsVertices and edges in elementsEdges:
for c=1:G.elementsEdgesSize
    for j=1:4
        G.elementsEdges(c,j)=evl(G.elementsEdges(c,j));
        G.elementsVertices(c,j)=vcl(G.elementsVertices(c,j));
    end
end
G.elementsEdges=G.elementsEdges(1:G.elementsEdgesSize,:);
% Save unique vertices, edges:
G.verticesCoordinates=verticesCoordinatesUnique; G.verticesCoordinatesSize=size(G.verticesCoordinates,1);
G.edgesVertices=edgesVerticesUnique; G.edgesVerticesSize=size(G.edgesVertices,1);
% All coordinates have been transformed, so delete parent element of
% vertices:
G.verticesParentElement=zeros(size(G.verticesParentElement));
% Shrink arrays to real size
G.elementsEdgesSize=size(G.elementsEdges,1);
G.elementsVertices=G.elementsVertices(1:G.elementsEdgesSize,:);
G.elementsChildren=G.elementsChildren(1:G.elementsEdgesSize,:);
G.elementsDegree=G.elementsDegree(1:G.elementsEdgesSize,:);
G.elementsEdges=G.elementsEdges(1:G.elementsEdgesSize,:);
G.elementsInMesh=G.elementsInMesh(1:G.elementsEdgesSize);
G.elementsParent=G.elementsParent(1:G.elementsEdgesSize);
G.elementsVertices=G.elementsVertices(1:G.elementsEdgesSize,:);
% Use new edges indices also for Dirichlet and Neumann edges:
G.edgesDirichlet=G.edgesDirichlet(evl2); G.edgesNeumann=G.edgesNeumann(evl2);
% Being a Dirichlet/Neumann edge is inherited to descendants:
for e=1:size(G.edgesVertices,1)
    u=G.edgesParent(e);
    if u~=0 && G.edgesDirichlet(u); G.edgesDirichlet(e)=1; end
    if u~=0 && G.edgesNeumann(u); G.edgesNeumann(e)=1; end
end
% Determine each element's descendants
G.elementsDescendants=cell(1,size(G.elementsVertices,1));
for parent=size(G.elementsVertices,1):-1:1
    G.elementsDescendants{parent}=sparse(1,size(G.elementsVertices,1));
    G.elementsDescendants{parent}(parent)=1;
    for ch=G.elementsChildren(parent,:)
        if ch==0; continue; end;
        G.elementsDescendants{parent}(ch)=1;
        G.elementsDescendants{parent}=or(G.elementsDescendants{parent},G.elementsDescendants{ch});
    end
end
for parent=1:size(G.elementsVertices,1)
    G.elementsDescendants{parent}=find(G.elementsDescendants{parent}==1);
end
% Determine each edge's orientation
% For a given edge in edges, return its orientation wrt the reference element.
% Orientation o is either in x direction (constants.X), or in y direction
% (constants.Y).
G.edgesOrientation=zeros(size(G.edgesVertices,1),1);
for edge=1:size(G.edgesVertices,1)
    [~,j]=find(G.elementsEdges==edge);
    switch j(1)
        case {1,3}; o=1; case {2,4}; o=2;
    end
    G.edgesOrientation(edge)=o;
end
% Tabularize reference element transformations
for T=1:G.elementsEdgesSize
    G.A{T}=matrixA(G.verticesCoordinates(G.elementsVertices(T,:),:));
    G.b{T}=vectorB(G.verticesCoordinates(G.elementsVertices(T,:),:));
end