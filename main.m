function [FEM,conMats,u]=essentials
addpath('./plotting'); addpath('./problems/ldomain_exp');

% parameters.iso determines number of refinements towards singularity:
parameters.problem='problems/ldomain_exp'; parameters.iso=10;
parameters.problemShortname='ldomain_exp'; parameters.levels=1;

constants.LEFT_NEIGHBOR=1; constants.RIGHT_NEIGHBOR=2;
constants.X=1; constants.Y=2;

FEM=initMesh(parameters);
FEM=performRefinements(FEM,constants,parameters);
FEM=finalizeRefinements(FEM,constants);
FEM=handleEdges(FEM);
FEM=handleVertices(FEM);
FEM=applyDegreeRule(FEM);
FEM=determineAssemblyDegree(FEM);
FEM=enumerateDofs(FEM);
conMats=computeConnectivityMatrices(FEM);
FEM.conMats=conMats;

addpath('./fem');
[FEM,conMats,u,h1RelativeError]=solvePoissonProblem(conMats,FEM,constants);

addpath('./plotting');
end