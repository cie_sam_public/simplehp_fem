function FEM=applyDegreeRule(FEM)
FEM.edgesOrientedDegree=zeros(size(FEM.edgesVertices,1),1);
FEM.edgesDegree=zeros(size(FEM.edgesVertices,1),2);
for e=find(FEM.edgesRegular)'
    neighbors=FEM.edgesNeighbors{e};
    neighborsDegree=FEM.elementsDegree(neighbors(neighbors>0),:);
    degreeFunction=@min;
    FEM.edgesDegree(e,:)=degreeFunction(neighborsDegree,[],1); % per-direction min/max |\label{degree:PerDirection}|
    FEM.edgesOrientedDegree(e)=FEM.edgesDegree(e,FEM.edgesOrientation(e)); % |\label{degree:edgesDegree}|
end
FEM.verticesDegree=ones(size(FEM.verticesCoordinates,1),2); % |\label{degree:vertices}|