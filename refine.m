function G=refine(c,typeOfRefinement,G,constants)
if length(typeOfRefinement)==1
    type=typeOfRefinement(1); 
    relFact=0.5;
elseif length(typeOfRefinement)==2
    type=typeOfRefinement(1); 
    relFact=typeOfRefinement(2);
end
% We subdivide in reference coordinates. After all refinements have been
% performed, we transform the coordinates to the corresponding elements.
% Parent element vertices
previousVerticesSize=G.verticesCoordinatesSize;
previousEdgesSize=G.edgesVerticesSize;
parentCoordinates=[-1 -1; 1 -1; 1 1; -1 1];
computeChildCoordinates=@(whichVertices,rels) sum(repmat(rels',1,2).*parentCoordinates(whichVertices,:),1);
switch type
    case 1
        numberOfChildElements=4;
        childCoordinates=[];
        tab=cell(1,numberOfChildElements);
        tab{1}={[1], [1 2], [1 2 3 4], [1 4]};
        tab{2}={[1 2], [2], [2 3], [1 2 3 4]};
        tab{3}={[1 2 3 4], [2 3], [3], [4 3]};
        tab{4}={[1 4], [1 2 3 4], [4 3], [4]};
        rel=cell(1,numberOfChildElements);
        rel{1}={[1],0.5*ones(1,2),0.25*ones(1,4),0.5*ones(1,2)};
        rel{2}={0.5*ones(1,2),[1],0.5*ones(1,2),0.25*ones(1,4)};
        rel{3}={0.5*ones(1,4),0.5*ones(1,2),[1],0.5*ones(1,2)};
        rel{4}={0.5*ones(1,2),0.25*ones(1,4),0.5*ones(1,2),[1]};
        for i=1:size(tab,2)
            for j=1:length(tab{i})
                childCoordinates(end+1,:)=computeChildCoordinates(tab{i}{j},rel{i}{j});
            end
        end
    case 2 % vertical cut
        numberOfChildElements=2;
        childCoordinates=[];
        tab=cell(1,numberOfChildElements);
        tab{1}={[1], [1 2], [4 3], [4]};
        tab{2}={[1 2], [2], [3], [4 3]};
        rel=cell(1,numberOfChildElements);
        rel{1}={[1],[1-relFact,relFact],[1-relFact,relFact],[1]};
        rel{2}={[1-relFact,relFact],[1],[1],[1-relFact,relFact]};
        for i=1:size(tab,2)
            for j=1:length(tab{i})
                childCoordinates(end+1,:)=computeChildCoordinates(tab{i}{j},rel{i}{j});
            end
        end
    case 3 % horizontal cut
        numberOfChildElements=2;
        childCoordinates=[];
        tab=cell(1,numberOfChildElements);
        tab{1}={[1],[2],[2 3],[1 4]};
        tab{2}={[1 4],[2 3], [3],[4]};
        rel=cell(1,numberOfChildElements);
        rel{1}={1,1,[1-relFact,relFact],[1-relFact,relFact]};
        rel{2}={[1-relFact,relFact],[1-relFact,relFact],[1],[1]};
        for i=1:size(tab,2)
            for j=1:length(tab{i})
                childCoordinates(end+1,:)=computeChildCoordinates(tab{i}{j},rel{i}{j});
            end
        end
    case constants.REFINEMENT_XY_WOBBLY % both directions, but with 
        
end
% Add child coordinates to verticesCoordinates (stiĺl in reference
% coordinates)
childCoordinatesSize=size(childCoordinates,1);
G.verticesCoordinates(previousVerticesSize+1:previousVerticesSize+childCoordinatesSize,:)=childCoordinates(1:childCoordinatesSize,:);
% Save element's parent in each vertex (for transformation)
G.verticesParentElement(G.verticesCoordinatesSize+1:G.verticesCoordinatesSize+childCoordinatesSize)=ones(1,childCoordinatesSize)*c;
childVertices = zeros(0,size(childCoordinates,1)); % Global indices of children in verticesCoordinates
childVertices(1:childCoordinatesSize)=previousVerticesSize+1:previousVerticesSize+childCoordinatesSize;
G.verticesDofNumber(childVertices(1:childCoordinatesSize)) = 0; G.verticesRegular(childVertices(1:childCoordinatesSize)) = 0;
G.verticesCoordinatesSize=childCoordinatesSize+G.verticesCoordinatesSize; 
% Ensure edges
childEdgesVertices=[];
edges=[1 2; 2 3; 4 3; 1 4];
tabEdges=cell(1,numberOfChildElements);
for i=1:size(tabEdges,2); tabEdges{i}=edges+(i-1)*4; end
for i=1:size(tabEdges,2)
    for j=1:length(tabEdges{i})
        childEdgesVertices(end+1,:)=previousVerticesSize+tabEdges{i}(j,:);
    end
end
% As above, add all "new" edges to the global edgesVertices list. These may
% be identified as existing edges later.
childEdgesSize=size(childEdgesVertices,1);
G.edgesVertices(G.edgesVerticesSize+1:G.edgesVerticesSize+childEdgesSize,:)=childEdgesVertices;%zeros(childEdgesSize,2);
G.edgesVerticesSize=childEdgesSize+G.edgesVerticesSize;

childEdges=(1:childEdgesSize)+previousEdgesSize;
G.edgesVertices(previousEdgesSize+1:previousEdgesSize+childEdgesSize,:)=childEdgesVertices;
G.edgesNeumann(childEdges,:) = zeros(childEdgesSize,1);
G.edgesDirichlet(childEdges,:) = zeros(childEdgesSize,1);

% Set parent of each new edge
switch type
    case 1
        tabEdgesParent=[1 1 1; 2 1 1; 2 2 2; 3 2 2; 3 3 3; 4 3 3; 4 4 4; 1 4 4];        
    case 2
        tabEdgesParent=[1 1 1; 1 3 3; 2 1 1; 2 3 3];
    case 3
        tabEdgesParent=[1 2 2; 1 4 4; 2 2 2; 2 4 4];
end
for i=1:size(tabEdgesParent,1)
    parentEdge=G.elementsEdges(c,tabEdgesParent(i,3));
    G.edgesParent(previousEdgesSize+tabEdgesParent(i,2)+4*(tabEdgesParent(i,1)-1))=parentEdge;
end
% Create new elements
elementSize=G.elementsEdgesSize;
for k=1:numberOfChildElements
    G.elementsVertices(elementSize+1,:)=(1:4)+4*(k-1)+previousVerticesSize;
    G.elementsEdges(elementSize+1,:)=(1:4)+(k-1)*4+previousEdgesSize;
    G.elementsInMesh(elementSize+1)=1;
    G.elementsParent(elementSize+1)=c;
    if constants.ISO==0
        deg=[randi([constants.MIN_DEG,constants.MAX_DEG]),randi([constants.MIN_DEG,constants.MAX_DEG])];
    else
        % inherit degree
        deg=G.elementsDegree(c,:);%constants.ISO*ones(1,2);
    end
    G.elementsChildren(elementSize+1,:)=[0 0 0 0]; %for error est
    G.elementsDegree(elementSize+1,:)=deg; %TODO autom
    elementSize=elementSize+1;
end
G.elementsEdgesSize=elementSize;

% Administrative work
G.elementsInMesh(c)=0; % Mark parent cell as inactive
switch type
    case 1
        G.elementsChildren(c,:) = G.elementsEdgesSize-[3,2,1,0]; % c has 4 children
    case 2
        G.elementsChildren(c,:) = [G.elementsEdgesSize-1,G.elementsEdgesSize,0,0]; % c has 2 children
    case 3
        G.elementsChildren(c,:) = [0,0,G.elementsEdgesSize-1,G.elementsEdgesSize]; % c has 2 children
end
end