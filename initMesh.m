function G=initMesh(parameters)
MAX=1e6; % maximum size of arrays
% Load coordinates, elements, and Dirichlet/Neumann edges (user-defined)
coordinatesFile=strcat(parameters.problem,'/coordinates.dat'); load(coordinatesFile); coordinates(:,1)=[];
elementsFile=strcat(parameters.problem,'/elements.dat'); load(elementsFile); elements(:,1)=[];
degreesFile=strcat(parameters.problem,'/degrees.dat'); load(degreesFile); degrees(:,1)=[];

G.verticesCoordinates=zeros(MAX,2); G.verticesCoordinates(1:size(coordinates,1),:)=coordinates;
G.verticesCoordinatesSize=size(coordinates,1);
G.verticesParentElement=zeros(MAX,1); 

G.edgesVertices = zeros(MAX,2); G.edgesVerticesSize=0;

% Create initial elements
G.elementsEdges=zeros(MAX,4); G.elementsEdgesSize=0; % count elements
G.elementsDegree=zeros(MAX,2); G.elementsInMesh=false(MAX,1);
G.elementsParent=zeros(MAX,1); G.elementsChildren=zeros(MAX,4);
G.elementsVertices=zeros(MAX,4);
for c=1:size(elements,1)
    curEl=elements(c,:);
    G.elementsVertices(c,:)=curEl; %4 vertices, counterclockwise
    edgesList=[1 2; 2 3; 4 3; 1 4]; %4 edges; shared edges have same orientation
    for i=1:4
        G.edgesVertices(G.edgesVerticesSize+1,:)=curEl(edgesList(i,:)); 
        G.edgesVerticesSize=1+G.edgesVerticesSize;
        G.elementsEdges(c,i)=G.edgesVerticesSize;        
    end
    G.elementsDegree(c,:)=degrees(c,:);
    G.elementsInMesh(c,:)=1;
    G.elementsEdgesSize=1+G.elementsEdgesSize;
end
G.edgesParent=zeros(MAX,1); G.edgesInMesh=zeros(MAX,1);
% Dirichlet and Neumann edges
G.edgesNeumann=zeros(MAX,1); G.edgesDirichlet=zeros(MAX,1);
dirichletFile=strcat(parameters.problem,'/dirichlet.dat'); load(dirichletFile); dirichlet(:,1)=[];
neumannFile=strcat(parameters.problem,'/neumann.dat'); load(neumannFile); neumann(:,1)=[];
for e=1:size(neumann,1)
    edge=neumann(e,:);
    [i,~]=find(ismember(G.edgesVertices(1:G.edgesVerticesSize,:),edge,'rows'));
    G.edgesNeumann(i)=1; 
end
for e=1:size(dirichlet,1)
    edge=dirichlet(e,:);
    [i,~]=find(ismember(G.edgesVertices(1:G.edgesVerticesSize,:),edge,'rows'));
    G.edgesDirichlet(i)=1;
end
% Set problem functions
G.functionF=@functionF; G.functionG=@functionG; G.functionDirichlet=@functionDirichlet;
G.functionU=@functionU; G.functionDu=@functionDu;
end