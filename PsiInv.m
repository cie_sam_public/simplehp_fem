% PsiInv is inverse of Psi. x is a row matrix whose lines are points to be
% transformed.
function result = PsiInv(x,T,G)
result=zeros(size(x)); for j=1:size(x,1); result(j,:)=G.A{T}\(x(j,:)'-G.b{T}'); end
end
 
