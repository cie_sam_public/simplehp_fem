function result = matrixA(coo)
result=[0.25*(coo(2,:)+coo(3,:)-coo(1,:)-coo(4,:))', 0.25*(coo(3,:)+coo(4,:)-coo(1,:)-coo(2,:))'];
end 
