function alphas=constraintsCoefficients2D(kQ,d,t,T,Q,FEM)
[diagonal,b]=Upsilon(T,Q,FEM);
Ld=L(d(1),d(2)); Lt=L(t(1),t(2)); % |\label{line:cc2d:L}|
alphas=cc1D(Ld(kQ,1),Lt(:,1),diagonal(1,1),b(1)).*cc1D(Ld(kQ,2),Lt(:,2),diagonal(2,2),b(2)); % |\label{line:cc2d:alphas}|
end 
function [diagonal,b]=Upsilon(T,Q,FEM) % Transformation $\Upsilon$ |\label{line:cc2d:Upsilon}|
coordinatesT=FEM.verticesCoordinates(FEM.elementsVertices(T,:),:);
vs=PsiInv([coordinatesT(1,:);coordinatesT(3,:)],Q,FEM);
diagonal=zeros(2); a=0.5*(vs(2,:)-vs(1,:)); diagonal(1,1)=a(1); diagonal(2,2)=a(2); b=0.5*(vs(2,:)+vs(1,:));
end
