function [support,bigElements]=getEdgeSupport_Large(e,FEM)
support={}; bigElements=[];
for c=1:2
  neighbor=FEM.edgesOriginalNeighbors{e}(c);
  if neighbor==0; continue, end;
  support{end+1}=FEM.elementsDescendants{neighbor}(FEM.elementsInMesh(FEM.elementsDescendants{neighbor}));
  bigElements(end+1)=neighbor;
end
end