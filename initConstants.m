function constants=initConstants(parameters)
    % Globals
    % -- Local basis functions
    constants={};
    constants.LEGENDRE = 1; constants.LAGRANGE=3; constants.MODIFIED_LAGRANGE=5;
    constants.POLY_TYPE = constants.LEGENDRE; %.MODIFIED_LAGRANGE;
    % -- Element types
    constants.AFFINE=1; constants.BILINEAR=2; 
    constants.TYPE=constants.AFFINE; %BILINEAR; %
    % -- Refinement types
    constants.REFINEMENT_PARAXIAL=1; constants.REFINEMENT_OTHER=2;
    constants.REFINEMENT_TYPE=constants.REFINEMENT_PARAXIAL;
    % -- Pre-defined algorithms
    constants.LARGE=1; constants.MEDIUM=2; constants.MINIMUM=3;
    % -- Polynomial degree distributions: if constants.ISO==0, assign at
    % random.
    constants.ISO=parameters.iso; constants.MIN_DEG=3; constants.MAX_DEG=5; 
    % -- Position of the left/right neighbor in matrices
    constants.LEFT_NEIGHBOR=1; constants.RIGHT_NEIGHBOR=2;
    % -- Position of the vertical/horizontal element degree in matrices
    % -- Edge orientation
    constants.X=1; constants.Y=2;
    % -- Short notation for possible subdivisions
    constants.SUBDIVISION_ISO=1; constants.SUBDIVISION_VERT=2; constants.SUBDIVISION_HORI=3; 
    % -- Tolerance value for exactness (mostly used in geometric operations)
    constants.TOL=1e-16; % TODO 1e-10 is a good value! 1e-16 is better. %eps(1)*2^((-100)*(sqrt(64/53)-1))
    % -- 
    constants.INITIAL_SIZE=1e6; % initial matrix size (for lists)
    constants.PLOT_STEP=0.30;  
    constants.NUM_DRAW_POINTS=10;%80; 
    % --- Additional integration knots (per direction)
    constants.NUM_ADDITIONAL_KNOTS=10;
end