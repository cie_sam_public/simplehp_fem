function result=cc1D(i,J,a,b)
result=zeros(1,length(J));
for t=1:length(J)
    j=J(t);
    if j >= 2 && j > i; continue; end;
    alphas = zeros(i+4,i+4);
    alphas(0+1,0+1) = (1+a-b)/2;
    alphas(1+1,0+1) = (1-a+b)/2;
    alphas(2+1,0+1) = (1-(a-b)^2)/2;
    alphas(0+1,1+1) = (1-a-b)/2;
    alphas(1+1,1+1) = (1+a+b)/2;
    alphas(2+1,1+1) = (1-(a+b)^2)/2;
    alphas(2+1,2+1) = a^2;
    for k=2:i-1
        alphas(k+1+1,0+1) = (b-a)*(2*k-1)/(k+1)*alphas(1+k,0+1) - ...
                        (k-2)/(k+1)*alphas(1+k-1,0+1);
        alphas(k+1+1,1+1) = (a+b)*(2*k-1)/(k+1)*alphas(1+k,1+1) - ...
                        (k-2)/(k+1)*alphas(1+k-1,1+1);
        alphas(k+1+1,2+1) = (2*k-1)/(k+1)*(a/5.0*alphas(1+k,3+1) + ...
                        b*alphas(1+k,2+1) + a*(alphas(1+k,0+1) - alphas(1+k,1+1))) ...
                        - (k-2)/(k+1)*alphas(1+k-1,2+1);
        for l=3:k-1
            alphas(k+1+1,l+1) = (2*k-1)/(k+1)*(a*l/(2*l-3)*alphas(1+k,l-1+1) ...
                    + a*(l-1)/(2*l+1)*alphas(1+k,l+1+1) + b*alphas(1+k,l+1))...
                    - (k-2)/(k+1)*alphas(1+k-1,l+1);
        end
        if k > 2
            alphas(k+1+1,k+1) = (2*k-1)/(k+1)*(a*k/(2*k-3)*alphas(1+k,k-1+1) ...
                    + b*alphas(1+k,k+1));
        end
        alphas(k+1+1,k+1+1) = a*alphas(1+k,k+1); 
    end
    result(t)=alphas(i+1,j+1);
end