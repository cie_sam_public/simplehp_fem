function connectivityMatrices=computeConnectivityMatrices(FEM)
connectivityMatrices=cell(1,size(FEM.elementsVertices,1));
for c=1:length(connectivityMatrices); connectivityMatrices{c}=sparse(FEM.numberOfDofs,prod(FEM.elementsAssemblyDegree(c,:)+1)); end
for v=find(FEM.verticesRegular)
  i=FEM.verticesDofNumber(v);
  [support,bigElements]=getVertexSupport_Large(v,FEM); % |\label{line:cm:Support}|
  for s=1:length(support)
    Q=bigElements(s); 
    kQ=getLocalDofNumber_Vertex(v,Q,FEM); % |\label{line:cm:kqVertex}|
    for T=support{s}
      connectivityMatrices{T}(i,:)=constraintsCoefficients2D(kQ,FEM.verticesDegree(v,:), ...
        FEM.elementsAssemblyDegree(T,:),T,Q,FEM); % |\label{line:cm:cc}|
    end
  end
end
for e=find(FEM.edgesRegular)'
  i=FEM.edgesDofNumber(e); if i==0; continue; end
  [support,bigElements]=getEdgeSupport_Large(e,FEM);
  for s=1:length(support)
    Q=bigElements(s); 
    kQ=getLocalDofNumber_Edge(e,FEM.edgesDegree(e,:),Q,FEM);
    for T=support{s}
      for l=0:FEM.edgesOrientedDegree(e)-2
        connectivityMatrices{T}(i+l,:)= ...
        constraintsCoefficients2D(kQ+l,FEM.edgesDegree(e,:),FEM.elementsAssemblyDegree(T,:),T,Q,FEM); % |\label{line:cm:ccEdge}|
      end
    end
  end
end
for T=find(FEM.elementsInMesh)'
  kQ=4+2*(FEM.elementsDegree(T,1)-1 + FEM.elementsDegree(T,2)-1)+1; % |\label{line:cm:offsetElement}|
  for l=0:(FEM.elementsDegree(T,1)-1)*(FEM.elementsDegree(T,2)-1)-1
    connectivityMatrices{T}(FEM.elementsDofNumber(T)+l,:)= ...
    constraintsCoefficients2D(kQ+l,FEM.elementsDegree(T,:),FEM.elementsAssemblyDegree(T,:),T,T,FEM); % |\label{line:cm:ccElement}|
  end
end
end

function kQ=getLocalDofNumber_Vertex(vertex,Q,FEM) % |\label{line:cm:getvertex}|
kQ=find(FEM.elementsVertices(Q,:)==vertex);
end
function kQ=getLocalDofNumber_Edge(e,degree,Q,FEM) % |\label{line:cm:getedge}|
localEdgeInQ=find(FEM.elementsEdges(Q,:)==e);
d1=degree(1); d2=degree(2);
offset1=5+d1-2; offset2=offset1+d2-1; offset3=offset2+d1-1;
switch localEdgeInQ
  case 1; kQ=5; case 2; kQ=offset1+1;
  case 3; kQ=offset2+1; case 4; kQ=offset3+1;
end
end