function [support,bigElements]=getVertexSupport_Large(v,FEM)
support={}; bigElements=[];
[allElementsContainingV,~]=find(FEM.elementsVertices==v); % |\label{line:vertexSupport:AllElements}|
for c=allElementsContainingV' % |\label{line:vertexSupport:Loop}|
  cIsBigElement=1; u=c;
  while u~=0
    u=FEM.elementsParent(u); if find(allElementsContainingV==u); cIsBigElement=0; break; end
  end
  if cIsBigElement==0; continue; end
  support{end+1}=FEM.elementsDescendants{c}(FEM.elementsInMesh(FEM.elementsDescendants{c})); % |\label{line:vertexSupport:Support}|
  bigElements(end+1)=c; % |\label{line:vertexSupport:bigElements}|
end

